﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using SimpleServerProto;

namespace NetHelper
{
    public static class SerializationHelper
    {
        static Encoding encoding = Encoding.UTF8;
        private static void SetValue(object obj, string name, byte[] data)
        {
        }
        public static byte[] EncodeString(string s)
        {
            if (s == null) s = "";
            byte[] utf8data = encoding.GetBytes(s);
            int length = utf8data.Length;
            byte[] result = new byte[sizeof(int) + length];
            Array.Copy(EncodeInt(length), result, sizeof(int));
            Array.Copy(utf8data, 0, result, sizeof(int), length);
            return result;
        }
        public static string DecodeString(byte[] data, int offset)
        {
            int length = DecodeInt(data, offset);
            return encoding.GetString(data, offset + sizeof(int), length);
        }
        public static void WriteString(Stream stream, string s)
        {
            byte[] data = EncodeString(s);
            stream.Write(data, 0, data.Length);
        }
        // Reads string length and data
        public static string ReadString(Stream stream)
        {
            byte[] data = new byte[sizeof(int)];
            stream.Read(data, 0, data.Length);
            int strLength = DecodeInt(data, 0);
            data = new byte[strLength];
            stream.Read(data, 0, strLength);
            return encoding.GetString(data);
        }

        public static byte[] EncodeUInt(uint i)
        {
            if (BitConverter.IsLittleEndian)
            {
                i = ((i & 0xFF) << 24) | ((i & 0xFF00) << 8) |
                    ((i & 0xFF0000) >> 8) | ((i & 0xFF000000) >> 24);
            }
            return BitConverter.GetBytes(i);
        }
        public static uint DecodeUInt(byte[] data, int offset)
        {
            uint i = BitConverter.ToUInt32(data, offset);
            if (BitConverter.IsLittleEndian)
            {
                i = ((i & 0xFF) << 24) | ((i & 0xFF00) << 8) |
                    ((i & 0xFF0000) >> 8) | ((i & 0xFF000000) >> 24);
            }
            return i;
        }
        public static uint ReadUInt(Stream stream)
        {
            byte[] data = new byte[sizeof(uint)];
            stream.Read(data, 0, sizeof(uint));
            return DecodeUInt(data, 0);
        }

        public static byte[] EncodeInt(int i)
        {
            if (BitConverter.IsLittleEndian)
            {
                bool sign = (i < 0);
                i = ((i & 0xFF) << 24) | ((i & 0xFF00) << 8) |
                    ((i & 0xFF0000) >> 8) | ((i & 0x7F000000) >> 24);
                if (sign) i |= 0x80;
            }
            return BitConverter.GetBytes(i);
        }
        public static int DecodeInt(byte[] data, int offset)
        {
            int i = 0;
            i = ((data[offset] & 0x7F) << 24) | (data[offset + 1] << 16) |
                (data[offset + 2] << 8) | (data[offset + 3]);
            if ((data[offset] & 0x80) != 0) i *= -1;
            return i;
        }
        public static int ReadInt(Stream stream)
        {
            byte[] data = new byte[sizeof(int)];
            stream.Read(data, 0, sizeof(int));
            return DecodeInt(data, 0);
        }

        public static bool ReadBool(Stream stream)
        {
            byte[] data = new byte[sizeof(bool)];
            stream.Read(data, 0, sizeof(bool));
            return BitConverter.ToBoolean(data, 0);
        }
        
        private static byte[] SerializeCookie(Cookie cookie)
        {
            byte[] strData = EncodeString(cookie.Name);
            MemoryStream ms = new MemoryStream();
            //ms.Write(typeName, 0, typeName.Length);
            ms.Write(strData, 0, strData.Length);
            ms.Write(EncodeUInt(cookie.Price), 0, sizeof(uint));
            return ms.ToArray();
        }
        private static int DeserializeCookie(Cookie cookie, byte[] data, int offset)
        {
            int nameLength = DecodeInt(data, offset);
            offset += sizeof(int);
            cookie.Name = encoding.GetString(data, offset, nameLength);
            offset += nameLength;
            cookie.Price = DecodeUInt(data, offset);
            offset += sizeof(uint);
            return offset;
        }
        private static Cookie ReadCookie(Stream stream)
        {
            Cookie cookie = new Cookie();
            cookie.Name = ReadString(stream);
            cookie.Price = ReadUInt(stream);
            return cookie;
        }

        private static byte[] SerializeRequestAuthorization(RequestAuthorization ra)
        {
            //byte[] typeName = EncodeString(ra.GetType().Name);
            byte[] strLogin = EncodeString(ra.UserId);
            byte[] strPassword = EncodeString(ra.Password);
            int dataLength = strLogin.Length + strPassword.Length;
            MemoryStream ms = new MemoryStream();
            //ms.Write(typeName, 0, typeName.Length);
            ms.Write(strLogin, 0, strLogin.Length);
            ms.Write(strPassword, 0, strPassword.Length);
            return ms.ToArray();
        }
        private static RequestAuthorization ReadRequestAuthorization(Stream stream)
        {
            RequestAuthorization req = new RequestAuthorization();
            req.UserId = ReadString(stream);
            req.Password = ReadString(stream);
            return req;
        }

        private static byte[] SerializeRequestBuyCookie(RequestBuyCookie rbc)
        {
            //byte[] typeName = EncodeString(rbc.GetType().Name);
            byte[] cookie = SerializeCookie(rbc.Cookie);
            int dataLength = cookie.Length + sizeof(uint);
            byte[] buffer = new byte[dataLength];
            MemoryStream ms = new MemoryStream(buffer);
            //ms.Write(typeName, 0, typeName.Length);
            ms.Write(cookie, 0, cookie.Length);
            ms.Write(EncodeUInt(rbc.Quantity), 0, sizeof(uint));
            return buffer;
        }
        private static RequestBuyCookie ReadRequestBuyCookie(Stream stream)
        {
            RequestBuyCookie req = new RequestBuyCookie();
            req.Cookie = ReadCookie(stream);
            req.Quantity = ReadUInt(stream);
            return req;
        }

        private static byte[] SerializeResponseAuthorization(ResponseAuthorization ra)
        {
            //byte[] typeName = EncodeString(ra.GetType().Name);
            MemoryStream ms = new MemoryStream();
            //ms.Write(typeName, 0, typeName.Length);
            ms.Write(BitConverter.GetBytes(ra.Error), 0, sizeof(bool));
            byte[] strError = EncodeString(ra.ErrorMessage);
            ms.Write(strError, 0, strError.Length);
            ms.Write(EncodeUInt(ra.Money), 0, sizeof(uint));
            if (ra.Cookies != null)
            {
                ms.Write(EncodeInt(ra.Cookies.Count), 0, sizeof(int));
                foreach (KeyValuePair<Cookie, uint> kvp in ra.Cookies)
                {
                    byte[] cookie = SerializeCookie(kvp.Key);
                    ms.Write(cookie, 0, cookie.Length);
                    ms.Write(EncodeUInt(kvp.Value), 0, sizeof(uint));
                }
            }
            else
                ms.Write(EncodeInt(0), 0, sizeof(int));
            return ms.ToArray();
        }
        private static ResponseAuthorization ReadResponseAuthorization(Stream stream)
        {
            ResponseAuthorization resp = new ResponseAuthorization();
            resp.Error = ReadBool(stream);
            resp.ErrorMessage = ReadString(stream);
            resp.Cookies = new Dictionary<Cookie, uint>();
            resp.Money = ReadUInt(stream);
            int cookiesCount = ReadInt(stream);
            for (int i = 0; i < cookiesCount; i++ )
            {
                Cookie c = ReadCookie(stream);
                uint q = ReadUInt(stream);
                resp.Cookies.Add(c, q);
            }
            return resp;
        }

        private static byte[] SerializeResponseBuyCookie(ResponseBuyCookie rbc)
        {
            //byte[] typeName = EncodeString(rbc.GetType().Name);
            MemoryStream ms = new MemoryStream();
            //ms.Write(typeName, 0, typeName.Length);
            ms.Write(BitConverter.GetBytes(rbc.Error), 0, sizeof(bool));
            byte[] strError = EncodeString(rbc.ErrorMessage);
            ms.Write(strError, 0, strError.Length);
            byte[] cookie = SerializeCookie(rbc.Cookie);
            ms.Write(cookie, 0, cookie.Length);
            ms.Write(EncodeUInt(rbc.Quantity), 0, sizeof(uint));
            return ms.ToArray();
        }
        private static ResponseBuyCookie ReadResponseBuyCookie(Stream stream)
        {
            ResponseBuyCookie resp = new ResponseBuyCookie();
            byte[] data = new byte[sizeof(bool)];
            stream.Read(data, 0, sizeof(bool));
            resp.Error = BitConverter.ToBoolean(data, 0);
            resp.ErrorMessage = ReadString(stream);
            resp.Cookie = ReadCookie(stream);
            resp.Quantity = ReadUInt(stream);
            return resp;
        }

        private static void WriteResponseCookieList(Stream stream, ResponseCookieList rcl)
        {
            WriteString(stream, rcl.GetType().Name);
        }
        private static byte[] SerializeResponseCookieList(ResponseCookieList rcl)
        {
            MemoryStream ms = new MemoryStream();
            //WriteResponseCookieList(ms, rcl);
            ms.Write(EncodeInt(rcl.Cookies.Count), 0, sizeof(int));
            foreach (Cookie cookie in rcl.Cookies)
            {
                byte[] cData = SerializeCookie(cookie);
                ms.Write(cData, 0, cData.Length);
            }
            return ms.ToArray();
        }
        private static ResponseCookieList ReadResponseCookieList(Stream stream)
        {
            ResponseCookieList resp = new ResponseCookieList();
            int nCookies = ReadInt(stream);
            resp.Cookies = new List<Cookie>();
            for (int i = 0; i < nCookies; i++ )
            {
                resp.Cookies.Add(ReadCookie(stream));
            }
            return resp;
        }

        private static byte[] SerializeData(object obj)
        {
            switch (obj.GetType().FullName)
            {
                case "SimpleServerProto.Cookie": 
                    return SerializeCookie(obj as Cookie);
                case "SimpleServerProto.RequestAuthorization": 
                    return SerializeRequestAuthorization(obj as RequestAuthorization);
                case "SimpleServerProto.RequestBuyCookie":
                    return SerializeRequestBuyCookie(obj as RequestBuyCookie);
                case "SimpleServerProto.ResponseAuthorization":
                    return SerializeResponseAuthorization(obj as ResponseAuthorization);
                case "SimpleServerProto.ResponseBuyCookie":
                    return SerializeResponseBuyCookie(obj as ResponseBuyCookie);
                case "SimpleServerProto.ResponseCookieList":
                    return SerializeResponseCookieList(obj as ResponseCookieList);
                default:
                    {
                        byte[] result = EncodeString(obj.GetType().Name);
                        return result;
                        //throw new NotImplementedException();
                    }
            }
        }
        public static void Serialize(Stream stream, object obj)
        {
            byte[] name = EncodeString(obj.GetType().Name);
            stream.Write(name, 0, name.Length);
            byte[] data = SerializeData(obj);
            byte[] dataLength = EncodeInt(data.Length);
            stream.Write(dataLength, 0, dataLength.Length);
            stream.Write(data, 0, data.Length);
        }
        public static object Deserialize(Stream stream)
        {
            string objType = ReadString(stream);
            int dataLength = ReadInt(stream);
            switch (objType)
            {
                case "Cookie": return ReadCookie(stream);
                case "RequestAuthorization": return ReadRequestAuthorization(stream);
                case "RequestBuyCookie": return ReadRequestBuyCookie(stream);
                case "ResponseAuthorization": return ReadResponseAuthorization(stream);
                case "ResponseBuyCookie": return ReadResponseBuyCookie(stream);
                case "ResponseCookieList": return ReadResponseCookieList(stream);
                default:
                    {
                        // Skip (unknown) data
                        byte[] data = new byte[dataLength];
                        stream.Read(data, 0, dataLength);
                        return objType;
                    }
            }
            //throw new NotImplementedException();
        }
    }
}
