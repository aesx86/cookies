﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;

namespace NetHelper
{
    public static class NetObject
    {
        public static void Send(Socket socket, object obj)
        {
            //client.Client.Send(SerializationHelper.Serialize(obj));
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            SerializationHelper.Serialize(ms, obj);
            //ms.CopyTo(socket.GetStream());
            byte[] data = ms.ToArray();
            byte[] dataSize = SerializationHelper.EncodeInt(data.Length);
            socket.Send(dataSize);
            socket.Send(data);
        }
        public static object Receive(Socket socket)
        {
            byte[] buffer = new byte[sizeof(int)];
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            int bytesRead, objSize;
            socket.Receive(buffer);
            objSize = SerializationHelper.DecodeInt(buffer, 0);
            buffer = new byte[objSize];
            do
            {
                bytesRead = socket.Receive(buffer);
                ms.Write(buffer, 0, bytesRead);
            } while (ms.Length < objSize);
            ms.Seek(0, System.IO.SeekOrigin.Begin);
            return SerializationHelper.Deserialize(ms);
        }
    }
}
