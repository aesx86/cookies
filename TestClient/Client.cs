﻿using System;
using System.Threading.Tasks;
//using System.Net;
using System.Net.Sockets;
using System.Xml;

using SimpleServerProto;
using NetHelper;
using System.Collections.Generic;

namespace TestClient
{
    class Client
    {
        static System.Net.IPAddress serverIP;
        static Int32 serverPort;
        static TcpClient client;
        static ResponseCookieList respCookies;
        static void Main(string[] args)
        {
            Console.WriteLine("Starting client");
            ReadConfig();

            Console.WriteLine("Connecting to {0}:{1}", serverIP, serverPort);
            client = new TcpClient(AddressFamily.InterNetwork);
            client.Connect(serverIP, serverPort);
            if (Authorize())
            {
                GetCookieList();
                while (true)
                {
                    ListCookies();
                    RequestBuyCookie reqBuyCookie = ChooseCookie();
                    if (reqBuyCookie == null) break;
                    BuyCookie(reqBuyCookie);
                }
            }
            client.Close();
        }
        static void BuyCookie(RequestBuyCookie rbc)
        {
            try
            {
                Send(rbc);
                Console.WriteLine("Sent RequestBuyCookie");
                object objReceived = Receive();
                ResponseBuyCookie resp = objReceived as ResponseBuyCookie;
                if (resp == null)
                {
                    Console.WriteLine("BuyCookie(): received {1}:{0}, expected ResponseBuyCookie", objReceived, objReceived.GetType().Name);
                    return;
                }
                if (resp.Error)
                {
                    Console.WriteLine("BuyCookie(): {0}", resp.ErrorMessage);
                    return;
                }
                Console.WriteLine("Bought {2} {0} cookies (each costs {1})", 
                    resp.Cookie.Name, resp.Cookie.Price, resp.Quantity);
            }
            catch (Exception e)
            {
                Console.WriteLine("BuyCookie(): {0}", e.Message);
            }
        }
        static RequestBuyCookie ChooseCookie()
        {
            while (true)
            {
                Console.Write("Cookie to buy (1-{0} or Enter to exit): ", respCookies.Cookies.Count);
                string s = Console.ReadLine();
                if (s == "")
                    break;
                int nCookie;
                if (!Int32.TryParse(s, out nCookie))
                {
                    Console.WriteLine("Enter cookie number");
                    continue;
                }
                if ((nCookie < 1) || (nCookie > respCookies.Cookies.Count))
                {
                    Console.WriteLine("Enter valid cookie number (1-{0})", respCookies.Cookies.Count);
                    continue;
                }
                uint nQuantity;
                while (true)
                {
                    Console.Write("How many? ", respCookies.Cookies.Count);
                    s = Console.ReadLine();
                    if (UInt32.TryParse(s, out nQuantity))
                        break;
                    Console.WriteLine("Enter valid number");
                }
                RequestBuyCookie result = new RequestBuyCookie();
                result.Cookie = respCookies.Cookies[nCookie - 1];
                result.Quantity = nQuantity;
                return result;
            }
            return null;
        }
        static void ListCookies()
        {
            if (respCookies.Cookies.Count != 0)
            {
                Console.WriteLine("Available cookies:");
                int idx = 1;
                foreach (Cookie cookie in respCookies.Cookies)
                {
                    Console.WriteLine("{0}. {1} ({2})", idx, cookie.Name, cookie.Price);
                    idx++;
                }
            }
            else
                Console.WriteLine("No cookies to buy");
        }
        static bool GetCookieList()
        {
            try
            {
                object objReceived = Receive();
                respCookies = objReceived as ResponseCookieList;
                if (respCookies == null)
                {
                    Console.WriteLine("GetCookieList(): received {1}:{0}, expected ResponseCookieList", objReceived, objReceived.GetType().Name);
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("GetCookieList(): {0}", e.Message);
            }
            return false;
        }
        static bool Authorize()
        {
            try
            {
                RequestAuthorization ra = new RequestAuthorization();
                Console.Write("User: ");
                ra.UserId = Console.ReadLine();
                Console.Write("Password: ");
                ra.Password = Console.ReadLine();
                Send(ra);
                Console.WriteLine("Sent RequestAuthorization");
                object objReceived = Receive();
                ResponseAuthorization respAuth = objReceived as ResponseAuthorization;
                if (respAuth == null)
                {
                    Console.WriteLine("Authorize(): received {1}:{0}, expected ResponseAuthorization", objReceived, objReceived.GetType().Name);
                    return false;
                }
                if (respAuth.Error)
                {
                    Console.WriteLine("Authorize(): {0}", respAuth.ErrorMessage);
                    return false;
                }
                Console.WriteLine("Authorized successfully. You have {0} credits", respAuth.Money);
                if (respAuth.Cookies.Count > 0)
                {
                    Console.WriteLine("You already have:");
                    foreach (KeyValuePair<Cookie, uint> kvp in respAuth.Cookies)
                    {
                        Console.WriteLine("  {0}: {1} cookies", kvp.Key.Name, kvp.Value);
                    }
                }
                Console.WriteLine();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Authorize(): {0}", e.Message);
            }
            return false;
        }
        static void ReadConfig()
        {
            using (XmlReader reader = XmlReader.Create("server_settings.xml"))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        string nodeName = reader.Name;
                        reader.Read();
                        switch (nodeName)
                        {
                            case "host": serverIP = System.Net.IPAddress.Parse(reader.Value); break;
                            case "port": serverPort = Convert.ToInt32(reader.Value); break;
                        }
                    }
                }
            }
        }
        static void Send(object obj)
        {
            NetHelper.NetObject.Send(client.Client, obj);
        }
        static object Receive()
        {
            return NetHelper.NetObject.Receive(client.Client);
        }
    }
}
