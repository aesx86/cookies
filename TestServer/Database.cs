﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SQLite;
using SimpleServerProto;

namespace TestServer
{
    static class Database
    {
        static string sConn = @"Data Source=cookies.db;Version=3;New=True;";

        public static void Init()
        {
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = "create table if not exists Cookies(id integer primary key, cookieName text varchar(64), cookiePrice integer, isActual bit);";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "create table if not exists Users(id integer primary key, accountName text varchar(64), accountPassword text varchar(64), accountCash integer, regTime datetime);";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "create table if not exists Orders(id integer primary key, accountID integer, cookieID integer, cookieQuantity integer, orderPrice integer, orderTime datetime);";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "update Cookies set isActual = 0;";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
        }

        // Returns NOT NULL if username/password match or new user is created
        // Returns NULL if username/password don't match DB record
        public static int? AddUser(string userName, string password)
        {
            int? userID = null;
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText =
@"insert into Users(accountName, accountPassword, accountCash, regTime) 
select @accountName, @accountPassword, @accountCash, CURRENT_TIMESTAMP
where not exists (select 1 from Users where (accountName = @accountName));";
                cmd.Parameters.Add("@accountName", System.Data.DbType.String).Value = userName;
                cmd.Parameters.Add("@accountPassword", System.Data.DbType.String).Value = password;
                cmd.Parameters.Add("@accountCash", System.Data.DbType.UInt32).Value = Server.InitialMoney;
                cmd.ExecuteNonQuery();

                cmd = conn.CreateCommand();
                cmd.CommandText = "select id, accountPassword from Users where (accountName = @accountName);";
                cmd.Parameters.Add("@accountName", System.Data.DbType.String).Value = userName;
                string pass = null;
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        userID = Convert.ToInt32(reader[0]);
                        pass = reader[1] as string;
                    }
                }
                if (pass != password) userID = null;
                conn.Close();
            }
            return userID;
        }

        public static int? AddCookie(string name, uint price)
        {
            int? cookieID = null;
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.Parameters.Add("@cookieName", System.Data.DbType.String).Value = name;
                cmd.Parameters.Add("@cookiePrice", System.Data.DbType.Int32).Value = price;
                cmd.CommandText = @"insert or replace into Cookies(id, cookieName, cookiePrice, isActual) 
values ((select id from Cookies where (cookieName = @cookieName)), @cookieName, @cookiePrice, 1);";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            return cookieID;
        }
        public static List<Cookie> GetCookieList()
        {
            List<Cookie> result = new List<Cookie>();
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = @"select cookieName, cookiePrice from Cookies where (isActual = 1)";
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Cookie cookie = new Cookie();
                        cookie.Name = reader[0] as string;
                        cookie.Price = Convert.ToUInt32(reader[1]);
                        result.Add(cookie);
                    }
                }
                conn.Close();
            }
            return result;
        }

        public static int? GetUserID(string userName, string password)
        {
            int? userID = null;
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = @"select id from Users where ((accountName = @accountName) and (accountPassword = @accountPassword));";
                cmd.Parameters.Add("@accountName", System.Data.DbType.String).Value = userName;
                cmd.Parameters.Add("@accountPassword", System.Data.DbType.String).Value = password;
                userID = (cmd.ExecuteScalar() as int?);
                conn.Close();
            }
            return userID;
        }

        public static uint? GetUserMoney(int userID)
        {
            uint? userMoney = null;
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = @"select accountCash from Users where (id = @userID);";
                cmd.Parameters.Add("@userID", System.Data.DbType.Int32).Value = userID;
                object m = cmd.ExecuteScalar();
                if (m != null)
                    userMoney = Convert.ToUInt32(m);
                conn.Close();
            }
            return userMoney;
        }

        public static Dictionary<Cookie, uint> GetUserCookies(int userID)
        {
            Dictionary<Cookie, uint> result = new Dictionary<Cookie, uint>();
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd = conn.CreateCommand();
                cmd.CommandText =
@"select Cookies.cookieName, Cookies.cookiePrice, sum(Orders.cookieQuantity)
from Cookies join Orders on (Cookies.id = Orders.cookieID)
where (Orders.accountID = @accountID)
group by Cookies.cookieName, Cookies.cookiePrice;";
                cmd.Parameters.Add("@accountID", System.Data.DbType.Int32).Value = userID;
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        SimpleServerProto.Cookie cookie = new SimpleServerProto.Cookie();
                        cookie.Name = reader[0] as string;
                        cookie.Price = Convert.ToUInt32(reader[1]);
                        uint count = Convert.ToUInt32(reader[2]);
                        result.Add(cookie, count);
                    }
                }
                conn.Close();
            }
            return result;
        }

        public static void BuyCookie(int userID, Cookie cookie, uint count)
        {
            using (SQLiteConnection conn = new SQLiteConnection(sConn))
            {
                conn.Open();
                SQLiteCommand cmd = conn.CreateCommand();
                cmd.CommandText = "select id from Cookies where ((cookieName = @cookieName) and (cookiePrice = @cookiePrice) and (isActual = 1))";
                cmd.Parameters.Add("@cookieName", System.Data.DbType.String).Value = cookie.Name;
                cmd.Parameters.Add("@cookiePrice", System.Data.DbType.String).Value = cookie.Price;
                int? cookieID = null;
                using (SQLiteDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        cookieID = Convert.ToInt32(reader[0]);
                    }
                }
                if (cookieID.HasValue)
                {
                    uint moneyAmount = cookie.Price * count;
                    cmd = conn.CreateCommand();
                    cmd.CommandText =
@"insert into Orders(accountID, cookieID, cookieQuantity, orderPrice, orderTime)
values (@userID, @cookieID, @cookieQuantity, @orderPrice, CURRENT_TIMESTAMP);
update Users set accountCash = accountCash - @orderPrice where (id = @userID);";
                    cmd.Parameters.Add("@userID", System.Data.DbType.Int32).Value = userID;
                    cmd.Parameters.Add("@cookieID", System.Data.DbType.Int32).Value = cookieID;
                    cmd.Parameters.Add("@cookieQuantity", System.Data.DbType.Int32).Value = count;
                    cmd.Parameters.Add("@orderPrice", System.Data.DbType.Int32).Value = moneyAmount;
                    cmd.ExecuteNonQuery();
                }
                conn.Close();
            }
        }
    }
}
