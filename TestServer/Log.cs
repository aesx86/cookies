﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    public static class Log
    {
        static System.IO.TextWriter writer;
        static void Init()
        {
            string logName = String.Format("server_{0}.log", DateTime.Now).Replace(":", "").Replace(' ', '_');
            writer = System.IO.File.CreateText(logName);
        }
        public static void Write(string format, params object[] args)
        {
            string logText = String.Format("{0} {1}", DateTime.Now.ToLongTimeString(), String.Format(format, args));
            Console.WriteLine(logText);
        
            if (writer == null) Init();
            writer.WriteLine(logText);
            writer.Flush();
        }
    }
}
