﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;
using SimpleServerProto;
using NetHelper;

namespace TestServer
{
    class ClientInstance
    {
        private TcpClient client;
        private int userID;

        public void ProcessClient(TcpClient tcpClient)
        {
            try
            {
                client = tcpClient;
                // AuthorizeClient() sets userID
                if (AuthorizeClient())
                {
                    SendCookieList();
                    while (true)
                    {
                        RequestBuyCookie reqBuyCookie = ReceiveObject() as RequestBuyCookie;
                        if (reqBuyCookie == null)
                            break;
                        ResponseBuyCookie rbc = BuyCookie(reqBuyCookie);
                        SendObject(rbc);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Write("ProcessClient(): {0}", e.Message);
            }
        }

        // Fills userID
        private bool AuthorizeClient()
        {
            object received = ReceiveObject();
            SimpleServerProto.RequestAuthorization request = received as RequestAuthorization;
            if (request == null)
            {
                Log.Write("AuthorizeClient(): received {0}, expected RequestAuthorization", received);
                return false;
            }
            SimpleServerProto.ResponseAuthorization response = new ResponseAuthorization();
            // Valid login/password?
            int? id = Database.AddUser(request.UserId, request.Password);
            if (!id.HasValue)
            {
                response.Error = true;
                response.ErrorMessage = "Invalid login/password";
                Log.Write("Failed login attempt for user {0}", request.UserId);
            }
            else
            {
                userID = id.Value;
                response.Cookies = Database.GetUserCookies(userID);
                uint? money = Database.GetUserMoney(id.Value);
                if (money.HasValue)
                    response.Money = money.Value;
                response.Error = false;
                Log.Write("User {0} logged in", request.UserId);
            }
            SendObject(response);
            return !response.Error;
        }

        private void SendCookieList()
        {
            SimpleServerProto.ResponseCookieList cookieList = new ResponseCookieList();
            cookieList.Cookies = Database.GetCookieList();
            SendObject(cookieList);
        }

        private ResponseBuyCookie BuyCookie(RequestBuyCookie rbc)
        {
            ResponseBuyCookie resp = new ResponseBuyCookie();
            uint? money = Database.GetUserMoney(userID);
            if (!money.HasValue)
            {
                resp.Error = true;
                resp.ErrorMessage = "Cannot read money information for this account";
                return resp;
            }
            resp.Cookie = rbc.Cookie;
            resp.Quantity = rbc.Quantity;
            uint needMoney = rbc.Cookie.Price * rbc.Quantity;
            if (money.Value < needMoney)
            {
                resp.Error = true;
                resp.ErrorMessage = "Insufficient funds";
                return resp;
            }
            Database.BuyCookie(userID, rbc.Cookie, rbc.Quantity);
            return resp;
        }

        void SendObject(object obj)
        {
            NetHelper.NetObject.Send(client.Client, obj);
        }
        object ReceiveObject()
        {
            return NetHelper.NetObject.Receive(client.Client);
        }
    }
}
