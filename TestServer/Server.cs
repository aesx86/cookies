﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;

using System.Xml;

using SimpleServerProto;
using NetHelper;

namespace TestServer
{
    class Server
    {
        static bool isRunning = false;
        static int nPort = 0;
        static IPAddress ipAddress = new IPAddress(0x0100007f);
        static int initialMoney = 0;

        public static int InitialMoney { get { return initialMoney; } }
        static Thread listeningThread;

        public static bool IsRunning { get { return isRunning; } }
        public static int Port { get { return nPort; } }
        public static IPAddress Host { get { return ipAddress; } }

        static void Main(string[] args)
        {
            try
            {
	            Init();
    	        while (IsRunning)
    	        {
                    // While listener accepts incoming connections, UI thread waits for Ctrl+C
                    Thread.Sleep(300);
    	        }
    	        Shutdown();
            }
            catch (Exception e)
            {
            	Log.Write(e.ToString());
            }
        }
        static void ListenerThread()
        {
            Log.Write("Listener thread started");
            try
            {
                TcpListener listener = new TcpListener(ipAddress, nPort);
                listener.Start();
                {
                    while (IsRunning)
                    {
                        while (listener.Pending())
                        {
                            TcpClient client = listener.AcceptTcpClient();
                            Thread clientThread = new Thread(new ParameterizedThreadStart(ProcessClient));
                            clientThread.Start(client);
                        }
                        Thread.Sleep(50);
                    }
                }
                listener.Stop();
            }
            catch (Exception e)
            {
                Log.Write("Listener thread: {0}", e.Message);
                isRunning = false;
            }
            Log.Write("Listener thread terminated");
        }
        static void Init()
        {
            Log.Write("Starting server");
            Console.CancelKeyPress += new ConsoleCancelEventHandler(OnCancelEvent);
            InitDatabase();
            ReadConfig();
            isRunning = true;
            try
            {
                listeningThread = new Thread(new ThreadStart(ListenerThread));
                listeningThread.Start();
                Log.Write("Server is listening on {0}:{1}, press Ctrl+C to stop", ipAddress, nPort);
            }
            catch (Exception e)
            {
                Log.Write(e.ToString());
            }
        }
        static void OnCancelEvent(object obj, ConsoleCancelEventArgs ccea)
        {
            isRunning = false;
            ccea.Cancel = true;
        }
        static void ReadConfig()
        {
            using (XmlReader reader = XmlReader.Create("server_settings.xml"))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        string nodeName = reader.Name;
                        reader.Read();
                        switch (nodeName)
                        {
                            case "host": ipAddress = IPAddress.Parse(reader.Value); break;
                            case "port": nPort = Convert.ToInt32(reader.Value); break;
                            case "initial_money": initialMoney = Convert.ToInt32(reader.Value); break;
                            case "cookies":
                                {
                                    while (reader.Read())
                                    {
                                        if ((reader.NodeType == XmlNodeType.EndElement) &&
                                            (reader.Name == "cookies"))
                                            break;
                                        if (reader.NodeType == XmlNodeType.Element)
                                            AddCookie(reader["id"], Convert.ToUInt32(reader["price"]));
                                    }
                                    break;
                                }
                        }
                    }
                }
            }
        }
        static void AddCookie(string name, uint price)
        {
            Database.AddCookie(name, price);
        }
        static void AddAccount(string login, string password)
        {
            Database.AddUser(login, password);
        }
        static void ProcessClient(object objClient)
        {
            TcpClient client = objClient as TcpClient;
            if (client == null)
            {
                Log.Write("ProcessClient: {0} is not a TcpClient", objClient);
                return;
            }
            Log.Write("ProcessClient: connection from {0}", client.Client.RemoteEndPoint);
            // process client loop
            {
                ClientInstance clientInst = new ClientInstance();
                clientInst.ProcessClient(client);
            }
            Log.Write("ProcessClient: closing connection from {0}", client.Client.RemoteEndPoint);
            client.Close();
        }
        static void InitDatabase()
        {
            Database.Init();
        }
        static void Shutdown()
        {
            Log.Write("Closing server");
            isRunning = false;
        }
    }
}
